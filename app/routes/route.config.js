System.register(['angular2/router', '../user/userprofile', '../home/home.component', '../user/clients/clients.component', '../user/client-details/client-details.component', '../user/product-details/product-details.component', '../user/products/products.component', '../user/funds/funds.component', '../user/fund-details/fund-details.component', '../user/fund-clients/fund-clients.component', '../user/transaction/transaction.component', '../user/transaction-details/transaction-details.component', '../user/instructions/instructions.component', '../user/instructions/track/track-instructions.component'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var router_1, userprofile_1, home_component_1, clients_component_1, client_details_component_1, product_details_component_1, products_component_1, funds_component_1, fund_details_component_1, fund_clients_component_1, transaction_component_1, transaction_details_component_1, instructions_component_1, track_instructions_component_1;
    var Routes, APP_ROUTES;
    return {
        setters:[
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (userprofile_1_1) {
                userprofile_1 = userprofile_1_1;
            },
            function (home_component_1_1) {
                home_component_1 = home_component_1_1;
            },
            function (clients_component_1_1) {
                clients_component_1 = clients_component_1_1;
            },
            function (client_details_component_1_1) {
                client_details_component_1 = client_details_component_1_1;
            },
            function (product_details_component_1_1) {
                product_details_component_1 = product_details_component_1_1;
            },
            function (products_component_1_1) {
                products_component_1 = products_component_1_1;
            },
            function (funds_component_1_1) {
                funds_component_1 = funds_component_1_1;
            },
            function (fund_details_component_1_1) {
                fund_details_component_1 = fund_details_component_1_1;
            },
            function (fund_clients_component_1_1) {
                fund_clients_component_1 = fund_clients_component_1_1;
            },
            function (transaction_component_1_1) {
                transaction_component_1 = transaction_component_1_1;
            },
            function (transaction_details_component_1_1) {
                transaction_details_component_1 = transaction_details_component_1_1;
            },
            function (instructions_component_1_1) {
                instructions_component_1 = instructions_component_1_1;
            },
            function (track_instructions_component_1_1) {
                track_instructions_component_1 = track_instructions_component_1_1;
            }],
        execute: function() {
            exports_1("Routes", Routes = {
                profile: new router_1.Route({ path: '/user/dashboard', name: 'User Profile', component: userprofile_1.UserProfile, useAsDefault: true }),
                home: new router_1.Route({ path: '/home', name: 'Home', component: home_component_1.HomeComponent }),
                clients: new router_1.Route({ path: '/user/clients', name: 'Clients', component: clients_component_1.ClientsComponent }),
                client_details: new router_1.Route({ path: '/user/client', name: 'Client Details', component: client_details_component_1.clientDetailsComponent }),
                product_details: new router_1.Route({ path: '/user/product', name: 'Product Details', component: product_details_component_1.productDetailsComponent }),
                products: new router_1.Route({ path: '/user/products', name: 'Products', component: products_component_1.ProductsComponent }),
                funds: new router_1.Route({ path: '/user/funds', name: 'Funds', component: funds_component_1.FundsComponent }),
                fund_details: new router_1.Route({ path: '/user/fund/details', name: 'Fund Details', component: fund_details_component_1.FundDetailsComponent }),
                fund_clients: new router_1.Route({ path: '/user/fund/clients', name: 'Fund Clients', component: fund_clients_component_1.fundClientsComponent }),
                transaction: new router_1.Route({ path: '/user/transaction', name: 'Transactions', component: transaction_component_1.TransactionComponent }),
                transaction_details: new router_1.Route({ path: '/user/transaction/view', name: 'Transaction Details', component: transaction_details_component_1.TransactionDetailsComponent }),
                instruction: new router_1.Route({ path: '/user/instruction', name: 'Instructions', component: instructions_component_1.InstructionComponent }),
                track_instruction: new router_1.Route({ path: '/user/instruction/track', name: 'Track Instructions', component: track_instructions_component_1.trackInstructions })
            });
            exports_1("APP_ROUTES", APP_ROUTES = Object.keys(Routes).map(function (r) { return Routes[r]; }));
        }
    }
});
//# sourceMappingURL=route.config.js.map