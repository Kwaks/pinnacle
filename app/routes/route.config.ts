import {Route} from 'angular2/router';
import {UserProfile} from '../user/userprofile';
import {HomeComponent} from '../home/home.component';

import {ClientsComponent} from '../user/clients/clients.component';
import {clientDetailsComponent} from '../user/client-details/client-details.component';

import {productDetailsComponent} from '../user/product-details/product-details.component';
import {ProductsComponent} from '../user/products/products.component';

import {FundsComponent} from '../user/funds/funds.component';
import {FundDetailsComponent} from '../user/fund-details/fund-details.component';
import {fundClientsComponent} from '../user/fund-clients/fund-clients.component';

import {TransactionComponent} from '../user/transaction/transaction.component';
import {TransactionDetailsComponent} from '../user/transaction-details/transaction-details.component';

import {InstructionComponent} from '../user/instructions/instructions.component';
import {trackInstructions} from '../user/instructions/track/track-instructions.component';



export var Routes = {
    profile: new Route({ path : '/user/dashboard', name: 'User Profile', component: UserProfile, useAsDefault: true}),
    home: new Route({ path : '/home', name: 'Home', component: HomeComponent}),

    clients: new Route({ path : '/user/clients', name: 'Clients', component: ClientsComponent}),
    client_details:new Route({ path : '/user/client', name: 'Client Details', component: clientDetailsComponent}),
    
    product_details: new Route({ path : '/user/product', name: 'Product Details', component: productDetailsComponent}),
    products: new Route({ path : '/user/products', name: 'Products', component: ProductsComponent}),
    
    funds: new Route({ path : '/user/funds', name: 'Funds', component: FundsComponent}),
    fund_details: new Route({ path: '/user/fund/details', name: 'Fund Details', component: FundDetailsComponent }),
    fund_clients: new Route({ path: '/user/fund/clients', name: 'Fund Clients', component: fundClientsComponent}),
    
    transaction: new Route({ path: '/user/transaction', name: 'Transactions', component: TransactionComponent}),
    transaction_details: new Route({ path: '/user/transaction/view', name: 'Transaction Details', component: TransactionDetailsComponent }),
    
    instruction: new Route({ path: '/user/instruction', name: 'Instructions', component: InstructionComponent}),
    track_instruction: new Route({ path: '/user/instruction/track', name: 'Track Instructions', component: trackInstructions})
     
}

export const APP_ROUTES = Object.keys(Routes).map(r => Routes[r]);