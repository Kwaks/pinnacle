import {Component} from 'angular2/core';
import {RouterLink, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {Routes, APP_ROUTES} from '../routes/route.config';
import {ClientsComponent} from '../user/clients/clients.component';

@Component({
    selector: 'my-home',
    templateUrl: 'app/home/home.html',
    directives: [ROUTER_DIRECTIVES]
})

export class HomeComponent {
    public routes;
    

    constructor() {
        this.routes = Routes;
    }
}