
import {provide} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {AppComponent} from "./app.component";
import {ROUTER_PROVIDERS,LocationStrategy,HashLocationStrategy} from 'angular2/router';
import {HTTP_PROVIDERS} from "angular2/http";
import {UserService} from './user/services/user.service';


bootstrap(AppComponent, [
    ROUTER_PROVIDERS, HTTP_PROVIDERS, UserService, provide(LocationStrategy, {useClass: HashLocationStrategy})
]);