import {Component, OnInit} from 'angular2/core';
import {UserService} from '../services/user.service';
import {RouterLink, ROUTER_DIRECTIVES, Router} from 'angular2/router';

@Component({
    selector: 'fund-clients',
    templateUrl: 'app/user/fund-clients/fund-clients.html',
    directives: [ROUTER_DIRECTIVES]
})

export class fundClientsComponent implements OnInit{
    private _clients = { clients: [], ids: [] };
    private _fund;
    
    constructor(private _userService: UserService, private _router: Router) {}
    
    ngOnInit():any {
        this._fund = this._userService.getFundInfo();
        
        this._userService.getProductFunds()
            .subscribe(
                data => {
                    for(var f = 0; f < data.funds.length; f++){
                        if( this._fund.fund_id === data.funds[f].fund_id){
                            this._clients.ids.push(data.funds[f]);
                        }
                    }
                },
                error => alert(JSON.stringify(error))
            );
        this._userService.getClients()
            .subscribe(
                data => {
                    for(var c = 0; c < data.clients.length; c++){
                        for(var i = 0; i < this._clients.ids.length; i++ ){
                            if( this._clients.ids[i].client_id === data.clients[c].client_id ){
                                this._clients.clients.push(data.clients[c]);
                                this._clients.clients[i].value = numeral(this._clients.clients[i].value).format('0,0.00');
                            }
                        }
                    }
                },
                error => alert(JSON.stringify(error))
            );
    }
    
    onClient(client:any){
        this._userService.storeClient(client);
        this._router.navigate(['Client Details']);
    }
}