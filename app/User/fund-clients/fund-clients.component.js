System.register(['angular2/core', '../services/user.service', 'angular2/router'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_service_1, router_1;
    var fundClientsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            fundClientsComponent = (function () {
                function fundClientsComponent(_userService, _router) {
                    this._userService = _userService;
                    this._router = _router;
                    this._clients = { clients: [], ids: [] };
                }
                fundClientsComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._fund = this._userService.getFundInfo();
                    this._userService.getProductFunds()
                        .subscribe(function (data) {
                        for (var f = 0; f < data.funds.length; f++) {
                            if (_this._fund.fund_id === data.funds[f].fund_id) {
                                _this._clients.ids.push(data.funds[f]);
                            }
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                    this._userService.getClients()
                        .subscribe(function (data) {
                        for (var c = 0; c < data.clients.length; c++) {
                            for (var i = 0; i < _this._clients.ids.length; i++) {
                                if (_this._clients.ids[i].client_id === data.clients[c].client_id) {
                                    _this._clients.clients.push(data.clients[c]);
                                    _this._clients.clients[i].value = numeral(_this._clients.clients[i].value).format('0,0.00');
                                }
                            }
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                };
                fundClientsComponent.prototype.onClient = function (client) {
                    this._userService.storeClient(client);
                    this._router.navigate(['Client Details']);
                };
                fundClientsComponent = __decorate([
                    core_1.Component({
                        selector: 'fund-clients',
                        templateUrl: 'app/user/fund-clients/fund-clients.html',
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], fundClientsComponent);
                return fundClientsComponent;
            }());
            exports_1("fundClientsComponent", fundClientsComponent);
        }
    }
});
//# sourceMappingURL=fund-clients.component.js.map