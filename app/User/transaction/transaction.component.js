System.register(['angular2/core', 'angular2/router', 'angular2/http'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, http_1;
    var TransactionComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            TransactionComponent = (function () {
                function TransactionComponent() {
                    this.instruction = {
                        product: "",
                        type: "",
                        status: "",
                        from_date: "",
                        to_date: ""
                    };
                    this.search_instructions = [
                        { investor: "Arnold Van Wyk", investor_id: "10609", submit_date: "25 Oct 2013 12:33", product: "Ashburton Corporate Endowment", type: "Add To Deal", status: "In Progress" },
                        { investor: "Amalia Janse Van Wyk", investor_id: "10606", submit_date: "18 Oct 2013 12:24", product: "Ashburton Corporate Endowment", type: "Add To Deal", status: "In Progress" },
                    ];
                    this.transactions = [
                        { date: '04 Nov 2013', type: 'Reinvestment Earned on Uncleared Funds', fund: 'Allan Gray Equity Fund', value: 16.34 },
                        { date: '01 Nov 2013', type: 'Debit Order', fund: 'Coronation Balanced Fund', value: 250.00 },
                        { date: '31 Oct 2013', type: 'Reinvestment', fund: 'Ashburton Equity Fund', value: 15.21 },
                        { date: '31 Oct 2013', type: 'Reinvestment', fund: 'Ashburton Balanced Fund', value: 3.76 },
                        { date: '31 Oct 2013', type: 'Reinvestment Earned on Uncleared Funds', fund: 'Investec Balanced Fund', value: 0.54 },
                        { date: '21 Oct 2013', type: 'Withdrawal', fund: 'Investec Equity Fund', value: -13.49 },
                        { date: '17 Oct 2013', type: 'Investment once off debit', fund: 'Sanlam Growth Fund', value: 1000.00 },
                        { date: '17 Oct 2013', type: 'Switch In', fund: 'Allan Gray Equity Fund', value: 491.15 },
                        { date: '17 Oct 2013', type: 'Switch Out', fund: '', value: -491.15 }
                    ];
                    this._search = false;
                }
                TransactionComponent.prototype.search = function () {
                    if (this._search == false) {
                        this._search = true;
                        this._searches = this.search_instructions.length.toString();
                    }
                    else {
                        this._search = false;
                    }
                };
                TransactionComponent = __decorate([
                    core_1.Component({
                        selector: 'my-transcations',
                        templateUrl: 'app/user/transaction/transaction.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [http_1.HTTP_PROVIDERS]
                    }), 
                    __metadata('design:paramtypes', [])
                ], TransactionComponent);
                return TransactionComponent;
            }());
            exports_1("TransactionComponent", TransactionComponent);
        }
    }
});
//# sourceMappingURL=transaction.component.js.map