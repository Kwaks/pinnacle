import {Component} from 'angular2/core';
import {RouterLink, ROUTER_DIRECTIVES} from 'angular2/router';
import {HTTP_PROVIDERS}    from 'angular2/http';

@Component({
	selector: 'my-transcations',
	templateUrl: 'app/user/transaction/transaction.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [HTTP_PROVIDERS]
})

export class TransactionComponent {
	public routes;
	_search:boolean;
	_searches:string;

	instruction = {
		product: "",
		type: "",
		status: "",
		from_date: "",
		to_date: ""
	};

	search_instructions = [
		{ investor: "Arnold Van Wyk", investor_id: "10609", submit_date: "25 Oct 2013 12:33", product: "Ashburton Corporate Endowment", type: "Add To Deal", status: "In Progress"},
		{ investor: "Amalia Janse Van Wyk", investor_id: "10606", submit_date: "18 Oct 2013 12:24", product: "Ashburton Corporate Endowment", type: "Add To Deal", status: "In Progress"},
		
	];

	private transactions = [
		{ date: '04 Nov 2013', type: 'Reinvestment Earned on Uncleared Funds', fund: 'Allan Gray Equity Fund', value: 16.34},
		{ date: '01 Nov 2013', type: 'Debit Order', fund: 'Coronation Balanced Fund', value: 250.00 },
		{ date: '31 Oct 2013', type: 'Reinvestment', fund: 'Ashburton Equity Fund', value: 15.21 },
		{ date: '31 Oct 2013', type: 'Reinvestment', fund: 'Ashburton Balanced Fund', value: 3.76 },
		{ date: '31 Oct 2013', type: 'Reinvestment Earned on Uncleared Funds', fund: 'Investec Balanced Fund', value: 0.54 },
		{ date: '21 Oct 2013', type: 'Withdrawal', fund: 'Investec Equity Fund', value: -13.49 },
		{ date: '17 Oct 2013', type: 'Investment once off debit', fund: 'Sanlam Growth Fund', value: 1000.00 },
		{ date: '17 Oct 2013', type: 'Switch In', fund: 'Allan Gray Equity Fund', value: 491.15 },
		{ date: '17 Oct 2013', type: 'Switch Out', fund: '', value: -491.15 }
	]
	constructor(){
		this._search = false;
	}

	search() {
		if(this._search == false){
			this._search = true;
			this._searches = this.search_instructions.length.toString();

		} else {
			this._search = false;
		}
	}
}