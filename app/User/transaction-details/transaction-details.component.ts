import {Component, OnInit} from 'angular2/core';
import {RouterLink, ROUTER_DIRECTIVES} from 'angular2/router';
import {UserService} from '../services/user.service'

@Component({
	selector: 'transaction-details',
	templateUrl: 'app/user/transaction-details/transaction-details.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [UserService]
})

export class TransactionDetailsComponent implements OnInit{

	private _transactions = {
		client: '',
		product: '',
		fund: '',
		transactions: []
	}
	
	constructor(private _userService: UserService) {}

	ngOnInit():any {
		let fund = this._userService.getFundInfo();
		this._transactions.client = this._userService.getClientInfo();
		this._transactions.product = this._userService.getProductInfo();
		this._transactions.fund = fund.name;
		
		this._userService.getFundTransactions()
			.subscribe(
				data => {
					for (var i = 0; i < data.transactions.length; i++) {
						if( fund.fund_id == data.transactions[i].fund_id ){
							this._transactions.transactions.push(data.transactions[i]);
						}
					}
				},
				error => alert(JSON.stringify(error)),
				() => {
					if(this._transactions.transactions.length > 0){
						for (var i = 0; i  < this._transactions.transactions.length; i++) {
							this._transactions.transactions[i].value= numeral(this._transactions.transactions[i].value).format('0,0.00');
						}
					}
				}
			);
		
	}
}