import {Component, OnInit} from 'angular2/core';
import {ROUTER_DIRECTIVES, Router} from 'angular2/router';
import {NgSelectOption} from 'angular2/common';
import {HTTP_PROVIDERS}    from 'angular2/http';
import {Routes, APP_ROUTES} from '../routes/route.config';
import {UserService} from './services/user.service';


@Component({
	templateUrl: 'app/user/userprofile.html',
	directives: [ROUTER_DIRECTIVES],
	providers:  [HTTP_PROVIDERS]
})

export class UserProfile implements OnInit{
	public routes;
	private _clients = { clients: [] };
	private _products = { products: [] };
	private _funds = { funds: [] };
	private _transactions = { transactions: [] };
    private _product = null;
    private rep_ccy = { ccy: "ZAR", value: 1 };
    private rates;
    private _ccys =  [ "ZAR", "USD", "GBP", "EUR", "CNY", "JPY"];

	constructor(private _userService: UserService, private _router: Router) {}

    ngOnInit():void {
    	this._userService.getClients()
    		.subscribe(
    			data => {
    				this._clients.clients = data.clients;
                    for (var index = 0; index < this._clients.clients.length; index++) {
                       this._clients.clients[index].value = numeral(this._clients.clients[index].value).format('0,0.00');
                       this._clients.clients[index].rep_ccy = data.clients[index].base_ccy;
                       this._clients.clients[index].rep_value = numeral(this._clients.clients[index].value).format('0,0.00');
                    }
    			},
    			error => alert(JSON.stringify(error))
    		);
        
    	this._userService.getProducts()
    		.subscribe(
    			data => {
    				this._products.products = data.products;
                    for (var index = 0; index < this._products.products.length; index++) {
                       this._products.products[index].value = numeral(this._products.products[index].value).format('0,0.00');
                       this._products.products[index].rep_ccy = this._products.products[index].base_ccy;
                       this._products.products[index].rep_value = numeral(this._products.products[index].value).format('0,0.00');
                    }
    			},
	    		error => alert(JSON.stringify(error))
    		);

    	this._userService.getFunds()
    		.subscribe(
    			data => {
    				this._funds.funds = data.funds;
                    for (var index = 0; index < this._funds.funds.length; index++) {
                       this._funds.funds[index].value = numeral(this._funds.funds[index].value).format('0,0.00');
                       this._funds.funds[index].rep_ccy = this._funds.funds[index].base_ccy;
                       this._funds.funds[index].rep_value = numeral(this._funds.funds[index].rep_value).format('0,0.00');
                    }
    			},
                error => alert(JSON.stringify(error))
            );
        
        this._userService.getCCYRates()
            .subscribe(
                data => {
                    this.rates = data.CCY_Rates;
                },
                error => alert(JSON.stringify(error))
            );
    }

    onClient(client){
        this._userService.storeClient(client);
        this._router.navigate(['Client Details']);
    }
    onProduct(product:any){
        this._userService.storeProduct(product);
        this._router.navigate(['Product Details']);
    }
    onFund(fund:any){
        this._userService.storeFund(fund);
        this._router.navigate(["Fund Clients"]);
    }

    onCCConversion(input:any, ccy:string){
       this.rep_ccy.ccy = ccy;
       input.value = numeral().unformat(input.value);
       
       if(input.ccy_symbol === "" || input.ccy_symbol){
        if(input.base_ccy === "ZAR"){
              for(var i = 0; i < this.rates[0].rates.length; i++){
                  if( this.rep_ccy.ccy === this.rates[0].rates[i].ccy){
                      this.rep_ccy.value = input.value * this.rates[0].rates[i].xrate;
                  }
              } 
        } else {
            for(var i = 0;i < this.rates[0].rates.length; i++){
                if( input.base_ccy === this.rates[0].rates[i].ccy ){
                    this.rep_ccy.value = ( input.value / this.rates[0].rates[i].xrate );
                    if( this.rep_ccy.ccy !== "ZAR" ){
                        for(var j = 0; j < this.rates[0].rates.length; j++){
                            if( this.rep_ccy.ccy === this.rates[0].rates[j].ccy){
                                this.rep_ccy.value = ( this.rep_ccy.value * this.rates[0].rates[j].xrate );
                            }
                        }
                    }
                }
            }
        }
        
        if(input.client_id){
            for (var c = 0; c < this._clients.clients.length; c++) {
                if(input.name === this._clients.clients[c].name ){
                    this._clients.clients[c].value = numeral(this._clients.clients[c].value).format('0,0.00');
                    this._clients.clients[c].rep_value = numeral(this.rep_ccy.value).format('0,0.00');
                    switch (ccy){
                        case "ZAR":
                          this._clients.clients[c].ccy_symbol = "R";
                          this._clients.clients[c].rep_ccy = "ZAR";
                          break;
                        case "USD":
                          this._clients.clients[c].ccy_symbol = "$";
                          this._clients.clients[c].rep_ccy = "USD";
                          break;
                        case "GBP":
                          this._clients.clients[c].ccy_symbol = "£";
                          this._clients.clients[c].rep_ccy = "GBP";
                          break;
                        case "EUR":
                          this._clients.clients[c].ccy_symbol = "€";
                          this._clients.clients[c].rep_ccy = "EUR";
                          break;
                        case "CNY":
                          this._clients.clients[c].ccy_symbol = "¥";
                          this._clients.clients[c].rep_ccy = "CNY";
                          break;
                        case "JPY":
                          this._clients.clients[c].ccy_symbol = "¥";
                          this._clients.clients[c].rep_ccy = "JPY";
                          break;
                    }
                }
            }
        } else if(input.product_id){
            for (var p = 0; p < this._products.products.length; p++) {
                if(input.name === this._products.products[p].name){
                    this._products.products[p].rep_value = numeral(this.rep_ccy.value.toFixed(2)).format('0,0.00');
                    this._products.products[p].value = numeral(this._products.products[p].value).format('0,0.00');
                    switch (ccy){
                        case "ZAR":
                          this._products.products[p].ccy_symbol = "R";
                          this._products.products[p].rep_ccy = "ZAR";
                          break;
                        case "USD":
                          this._products.products[p].ccy_symbol = "$";
                          this._products.products[p].rep_ccy = "USD";
                          break;
                        case "GBP":
                          this._products.products[p].ccy_symbol = "£";
                          this._products.products[p].rep_ccy = "GBP";
                          break;
                        case "EUR":
                          this._products.products[p].ccy_symbol = "€";
                          this._products.products[p].rep_ccy = "EUR";
                          break;
                        case "CNY":
                          this._products.products[p].ccy_symbol = "¥";
                          this._products.products[p].rep_ccy = "CNY";
                          break;
                        case "JPY":
                          this._products.products[p].ccy_symbol = "¥";
                          this._products.products[p].rep_ccy = "JPY";
                          break;
                    }
                }
            }
        } else if(input.fund_id){
            for (var f = 0; f < this._funds.funds.length; f++) {
                if(input.name === this._funds.funds[f].name){
                    this._funds.funds[f].rep_value = numeral(this.rep_ccy.value.toFixed(2)).format('0,0.00');
                    this._funds.funds[f].value = numeral(this._funds.funds[f].value).format('0,0.00');
                    switch (ccy){
                        case "ZAR":
                          this._funds.funds[f].ccy_symbol = "R";
                          this._funds.funds[f].rep_ccy = "ZAR";
                          break;
                        case "USD":
                          this._funds.funds[f].ccy_symbol = "$";
                          this._funds.funds[f].rep_ccy = "USD";
                          break;
                        case "GBP":
                          this._funds.funds[f].ccy_symbol = "£";
                          this._funds.funds[f].rep_ccy = "GBP";
                          break;
                        case "EUR":
                          this._funds.funds[f].ccy_symbol = "€";
                          this._funds.funds[f].rep_ccy = "EUR";
                          break;
                        case "CNY":
                          this._funds.funds[f].ccy_symbol = "¥";
                          this._funds.funds[f].rep_ccy = "CNY";
                          break;
                        case "JPY":
                          this._funds.funds[f].ccy_symbol = "¥";
                          this._funds.funds[f].rep_ccy = "JPY";
                          break;
                    }
                }
            }
        }   
       }
       
       if(input.client_id){
        if(ccy === input.base_ccy){
            this._userService.getClients()
                .subscribe(
                    data => {
                        for(var i = 0; i < data.clients.length; i++){
                            if( data.clients[i].client_id === input.client_id ){
                                this._clients.clients[i] = data.clients[i];
                                this._clients.clients[i].value = numeral(this._clients.clients[i].value).format('0,0.00');
                                this._clients.clients[i].rep_ccy = data.clients[i].base_ccy;
                                this._clients.clients[i].rep_value = numeral(this._clients.clients[i].value).format('0,0.00');
                            }
                        }
                        
                    },
                    error => alert(JSON.stringify(error))
                );
        }
       }else if(input.product_id){
                if(ccy === input.base_ccy){
                    this._userService.getProducts()
                        .subscribe(
                            data => {
                                for(var i = 0; i < data.products.length; i++){
                                    if( data.products[i].product_id === input.product_id ){
                                        this._products.products[i] = data.products[i];
                                        this._products.products[i].value = numeral(this._products.products[i].value).format('0,0.00');
                                        this._products.products[i].rep_ccy = data.products[i].base_ccy;
                                        this._products.products[i].rep_value = numeral(this._products.products[i].value).format('0,0.00');
                                        
                                    }
                                }
                            },
                            error => alert(JSON.stringify(error))
                        );
                }
       }else if(input.fund_id){
                if(ccy === input.base_ccy){
                    this._userService.getFunds()
                        .subscribe(
                            data => {
                                for(var i = 0; i < data.funds.length; i++){
                                    if( data.funds[i].fund_id === input.fund_id ){
                                        this._funds.funds[i] = data.funds[i];
                                        this._funds.funds[i].value = numeral(this._funds.funds[i].value).format('0,0.00');
                                        this._funds.funds[i].rep_ccy = data.funds[i].base_ccy;
                                        this._funds.funds[i].rep_value = numeral(this._funds.funds[i].value).format('0,0.00');
                                    }
                                }
                            },
                            error => alert(JSON.stringify(error))
                        );
                }
        }
    }
}