System.register(['angular2/core', 'angular2/router', '../services/user.service'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, user_service_1;
    var ClientsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            ClientsComponent = (function () {
                function ClientsComponent(_userService, _router) {
                    this._userService = _userService;
                    this._router = _router;
                    this._clients = { clients: [] };
                }
                ClientsComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._userService.getClients()
                        .subscribe(function (data) {
                        _this._clients.clients = data.clients;
                    }, function (error) { return alert(JSON.stringify(error)); }, function () {
                        for (var i = 0; i < _this._clients.clients.length; i++) {
                            _this._clients.clients[i].value = numeral(_this._clients.clients[i].value).format('0,0.00');
                        }
                    });
                };
                ClientsComponent.prototype.onClient = function (client) {
                    this._userService.storeClient(client);
                    this._router.navigate(['Client Details']);
                };
                ClientsComponent = __decorate([
                    core_1.Component({
                        selector: 'my-clients',
                        templateUrl: 'app/user/clients/clients.html',
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], ClientsComponent);
                return ClientsComponent;
            }());
            exports_1("ClientsComponent", ClientsComponent);
        }
    }
});
//# sourceMappingURL=clients.component.js.map