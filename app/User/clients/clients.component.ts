import {Component, OnInit} from 'angular2/core';
import {RouterLink, ROUTER_DIRECTIVES, Router} from 'angular2/router';
import {UserService} from '../services/user.service';

@Component({
    selector: 'my-clients',
    templateUrl: 'app/user/clients/clients.html',
    directives: [ROUTER_DIRECTIVES]
})

export class ClientsComponent implements OnInit{
    private _clients = { clients: [] };
	
	constructor(private _userService: UserService, private _router: Router){}
	
	ngOnInit():any{
		this._userService.getClients()
			.subscribe(
				data => {
					this._clients.clients = data.clients;
				},
				error => alert(JSON.stringify(error)),
				() => {
					for (var i = 0; i < this._clients.clients.length; i++) {
						this._clients.clients[i].value = numeral(this._clients.clients[i].value).format('0,0.00');
					}
				}
			);
	}
	onClient(client){
        this._userService.storeClient(client);
        this._router.navigate(['Client Details']);
    }
}