import {Pipe, PipeTransform} from 'angular2/core';
import {User} from '../models/user.model';

@Pipe({
	name: 'UserSearch'
})

export class UserSearch implements PipeTransform {

	transform(value:User[], args:any[]){
		console.log(args);
		console.log(value);

		let arg = args[0].toString();
		//return value.filter((item)=> item.fNumber.toLowerCase().indexOf(args.toLowerCase()) != -1);
		function searchString(item){
			return (item.fNumber.toLowerCase().indexOf(arg.toLowerCase()) != -1 || item.fullName.toLowerCase().indexOf(arg.toLowerCase()) != -1 );
		}
		if(value != undefined) {
			return value.filter(searchString);
		} else {
			return value;
		}
	}
}