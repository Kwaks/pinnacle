System.register(['angular2/core'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var UserSearch;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            UserSearch = (function () {
                function UserSearch() {
                }
                UserSearch.prototype.transform = function (value, args) {
                    console.log(args);
                    console.log(value);
                    var arg = args[0].toString();
                    //return value.filter((item)=> item.fNumber.toLowerCase().indexOf(args.toLowerCase()) != -1);
                    function searchString(item) {
                        return (item.fNumber.toLowerCase().indexOf(arg.toLowerCase()) != -1 || item.fullName.toLowerCase().indexOf(arg.toLowerCase()) != -1);
                    }
                    if (value != undefined) {
                        return value.filter(searchString);
                    }
                    else {
                        return value;
                    }
                };
                UserSearch = __decorate([
                    core_1.Pipe({
                        name: 'UserSearch'
                    }), 
                    __metadata('design:paramtypes', [])
                ], UserSearch);
                return UserSearch;
            }());
            exports_1("UserSearch", UserSearch);
        }
    }
});
//# sourceMappingURL=user-search.pipe.js.map