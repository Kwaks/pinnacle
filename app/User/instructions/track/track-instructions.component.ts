import {Component} from 'angular2/core';
import {RouterLink, ROUTER_DIRECTIVES} from 'angular2/router';
import {HTTP_PROVIDERS}    from 'angular2/http';

@Component({
	selector: 'track-instructions',
	templateUrl: 'app/user/instructions/track/track-instructions.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [HTTP_PROVIDERS]
})

export class trackInstructions{

	constructor(){
		
	}


	_search:boolean;
	_searches:string;

	private instruction = {
		product: "",
		type: "",
		status: "",
		from_date: "",
		to_date: ""
	};

	search_instructions = [
		{ investor: "Arnold Van Wyk", investor_id: "10609", submit_date: "25 Oct 2013 12:33", product: "Ashburton Corporate Endowment", type: "Add To Deal", status: "In Progress"},
		{ investor: "Amalia Janse Van Wyk", investor_id: "10606", submit_date: "18 Oct 2013 12:24", product: "Ashburton Corporate Endowment", type: "Add To Deal", status: "In Progress"},
		
	];

	search() {
		if(this._search == false){
			this._search = true;
			this._searches = this.search_instructions.length.toString();

		} else {
			this._search = false;
		}
	}
}