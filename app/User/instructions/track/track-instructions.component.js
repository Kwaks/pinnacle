System.register(['angular2/core', 'angular2/router', 'angular2/http'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, http_1;
    var trackInstructions;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            trackInstructions = (function () {
                function trackInstructions() {
                    this.instruction = {
                        product: "",
                        type: "",
                        status: "",
                        from_date: "",
                        to_date: ""
                    };
                    this.search_instructions = [
                        { investor: "Arnold Van Wyk", investor_id: "10609", submit_date: "25 Oct 2013 12:33", product: "Ashburton Corporate Endowment", type: "Add To Deal", status: "In Progress" },
                        { investor: "Amalia Janse Van Wyk", investor_id: "10606", submit_date: "18 Oct 2013 12:24", product: "Ashburton Corporate Endowment", type: "Add To Deal", status: "In Progress" },
                    ];
                }
                trackInstructions.prototype.search = function () {
                    if (this._search == false) {
                        this._search = true;
                        this._searches = this.search_instructions.length.toString();
                    }
                    else {
                        this._search = false;
                    }
                };
                trackInstructions = __decorate([
                    core_1.Component({
                        selector: 'track-instructions',
                        templateUrl: 'app/user/instructions/track/track-instructions.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [http_1.HTTP_PROVIDERS]
                    }), 
                    __metadata('design:paramtypes', [])
                ], trackInstructions);
                return trackInstructions;
            }());
            exports_1("trackInstructions", trackInstructions);
        }
    }
});
//# sourceMappingURL=track-instructions.component.js.map