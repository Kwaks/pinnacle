import {Component} from 'angular2/core';
import {RouterLink, ROUTER_DIRECTIVES} from 'angular2/router';
import {HTTP_PROVIDERS}    from 'angular2/http';

@Component({
	selector: 'my-instructions',
	templateUrl: 'app/user/instructions/instructions.html',
	directives: [ROUTER_DIRECTIVES],
	providers: [HTTP_PROVIDERS]
})

export class InstructionComponent {
	public routes;

	constructor(){
		
	}


	private instructions = [
		{ type: 'Add To Deal', product_name: 'Ashburton Corporate Endowment', status: 'In Progress', count: 2 },
		{ type: 'New Business', product_name: 'Ashburton Corporate Endowment', status: 'In Progress', count: 2 },
		{ type: 'New Business', product_name: 'Ashburton Corporate Endowment', status: 'Outsanding Requirements', count: 1 },
		{ type: 'Additions', product_name: 'Ashburton Pension Preservation', status: 'In Progress', count: 1 },
		{ type: 'Disinvestment', product_name: 'Ashburton Pension Preservation', status: 'In Progress', count: 1 },
		{ type: 'FSP/IFA Maintaince', product_name: 'Ashburton Pension Preservation', status: 'In Progress', count: 1 },
		{ type: 'New Business', product_name: 'Ashburton Pension Preservation', status: 'In Progress', count: 2 }
	]


}