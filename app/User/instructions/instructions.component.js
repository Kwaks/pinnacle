System.register(['angular2/core', 'angular2/router', 'angular2/http'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, http_1;
    var InstructionComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            InstructionComponent = (function () {
                function InstructionComponent() {
                    this.instructions = [
                        { type: 'Add To Deal', product_name: 'Ashburton Corporate Endowment', status: 'In Progress', count: 2 },
                        { type: 'New Business', product_name: 'Ashburton Corporate Endowment', status: 'In Progress', count: 2 },
                        { type: 'New Business', product_name: 'Ashburton Corporate Endowment', status: 'Outsanding Requirements', count: 1 },
                        { type: 'Additions', product_name: 'Ashburton Pension Preservation', status: 'In Progress', count: 1 },
                        { type: 'Disinvestment', product_name: 'Ashburton Pension Preservation', status: 'In Progress', count: 1 },
                        { type: 'FSP/IFA Maintaince', product_name: 'Ashburton Pension Preservation', status: 'In Progress', count: 1 },
                        { type: 'New Business', product_name: 'Ashburton Pension Preservation', status: 'In Progress', count: 2 }
                    ];
                }
                InstructionComponent = __decorate([
                    core_1.Component({
                        selector: 'my-instructions',
                        templateUrl: 'app/user/instructions/instructions.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [http_1.HTTP_PROVIDERS]
                    }), 
                    __metadata('design:paramtypes', [])
                ], InstructionComponent);
                return InstructionComponent;
            }());
            exports_1("InstructionComponent", InstructionComponent);
        }
    }
});
//# sourceMappingURL=instructions.component.js.map