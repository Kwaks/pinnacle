System.register(['angular2/core', 'angular2/router', 'angular2/http', './services/user.service'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, http_1, user_service_1;
    var UserProfile;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            UserProfile = (function () {
                function UserProfile(_userService, _router) {
                    this._userService = _userService;
                    this._router = _router;
                    this._clients = { clients: [] };
                    this._products = { products: [] };
                    this._funds = { funds: [] };
                    this._transactions = { transactions: [] };
                    this._product = null;
                    this.rep_ccy = { ccy: "ZAR", value: 1 };
                    this._ccys = ["ZAR", "USD", "GBP", "EUR", "CNY", "JPY"];
                }
                UserProfile.prototype.ngOnInit = function () {
                    var _this = this;
                    this._userService.getClients()
                        .subscribe(function (data) {
                        _this._clients.clients = data.clients;
                        for (var index = 0; index < _this._clients.clients.length; index++) {
                            _this._clients.clients[index].value = numeral(_this._clients.clients[index].value).format('0,0.00');
                            _this._clients.clients[index].rep_ccy = data.clients[index].base_ccy;
                            _this._clients.clients[index].rep_value = numeral(_this._clients.clients[index].value).format('0,0.00');
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                    this._userService.getProducts()
                        .subscribe(function (data) {
                        _this._products.products = data.products;
                        for (var index = 0; index < _this._products.products.length; index++) {
                            _this._products.products[index].value = numeral(_this._products.products[index].value).format('0,0.00');
                            _this._products.products[index].rep_ccy = _this._products.products[index].base_ccy;
                            _this._products.products[index].rep_value = numeral(_this._products.products[index].value).format('0,0.00');
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                    this._userService.getFunds()
                        .subscribe(function (data) {
                        _this._funds.funds = data.funds;
                        for (var index = 0; index < _this._funds.funds.length; index++) {
                            _this._funds.funds[index].value = numeral(_this._funds.funds[index].value).format('0,0.00');
                            _this._funds.funds[index].rep_ccy = _this._funds.funds[index].base_ccy;
                            _this._funds.funds[index].rep_value = numeral(_this._funds.funds[index].rep_value).format('0,0.00');
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                    this._userService.getCCYRates()
                        .subscribe(function (data) {
                        _this.rates = data.CCY_Rates;
                    }, function (error) { return alert(JSON.stringify(error)); });
                };
                UserProfile.prototype.onClient = function (client) {
                    this._userService.storeClient(client);
                    this._router.navigate(['Client Details']);
                };
                UserProfile.prototype.onProduct = function (product) {
                    this._userService.storeProduct(product);
                    this._router.navigate(['Product Details']);
                };
                UserProfile.prototype.onFund = function (fund) {
                    this._userService.storeFund(fund);
                    this._router.navigate(["Fund Clients"]);
                };
                UserProfile.prototype.onCCConversion = function (input, ccy) {
                    var _this = this;
                    this.rep_ccy.ccy = ccy;
                    input.value = numeral().unformat(input.value);
                    if (input.ccy_symbol === "" || input.ccy_symbol) {
                        if (input.base_ccy === "ZAR") {
                            for (var i = 0; i < this.rates[0].rates.length; i++) {
                                if (this.rep_ccy.ccy === this.rates[0].rates[i].ccy) {
                                    this.rep_ccy.value = input.value * this.rates[0].rates[i].xrate;
                                }
                            }
                        }
                        else {
                            for (var i = 0; i < this.rates[0].rates.length; i++) {
                                if (input.base_ccy === this.rates[0].rates[i].ccy) {
                                    this.rep_ccy.value = (input.value / this.rates[0].rates[i].xrate);
                                    if (this.rep_ccy.ccy !== "ZAR") {
                                        for (var j = 0; j < this.rates[0].rates.length; j++) {
                                            if (this.rep_ccy.ccy === this.rates[0].rates[j].ccy) {
                                                this.rep_ccy.value = (this.rep_ccy.value * this.rates[0].rates[j].xrate);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (input.client_id) {
                            for (var c = 0; c < this._clients.clients.length; c++) {
                                if (input.name === this._clients.clients[c].name) {
                                    this._clients.clients[c].value = numeral(this._clients.clients[c].value).format('0,0.00');
                                    this._clients.clients[c].rep_value = numeral(this.rep_ccy.value).format('0,0.00');
                                    switch (ccy) {
                                        case "ZAR":
                                            this._clients.clients[c].ccy_symbol = "R";
                                            this._clients.clients[c].rep_ccy = "ZAR";
                                            break;
                                        case "USD":
                                            this._clients.clients[c].ccy_symbol = "$";
                                            this._clients.clients[c].rep_ccy = "USD";
                                            break;
                                        case "GBP":
                                            this._clients.clients[c].ccy_symbol = "£";
                                            this._clients.clients[c].rep_ccy = "GBP";
                                            break;
                                        case "EUR":
                                            this._clients.clients[c].ccy_symbol = "€";
                                            this._clients.clients[c].rep_ccy = "EUR";
                                            break;
                                        case "CNY":
                                            this._clients.clients[c].ccy_symbol = "¥";
                                            this._clients.clients[c].rep_ccy = "CNY";
                                            break;
                                        case "JPY":
                                            this._clients.clients[c].ccy_symbol = "¥";
                                            this._clients.clients[c].rep_ccy = "JPY";
                                            break;
                                    }
                                }
                            }
                        }
                        else if (input.product_id) {
                            for (var p = 0; p < this._products.products.length; p++) {
                                if (input.name === this._products.products[p].name) {
                                    this._products.products[p].rep_value = numeral(this.rep_ccy.value.toFixed(2)).format('0,0.00');
                                    this._products.products[p].value = numeral(this._products.products[p].value).format('0,0.00');
                                    switch (ccy) {
                                        case "ZAR":
                                            this._products.products[p].ccy_symbol = "R";
                                            this._products.products[p].rep_ccy = "ZAR";
                                            break;
                                        case "USD":
                                            this._products.products[p].ccy_symbol = "$";
                                            this._products.products[p].rep_ccy = "USD";
                                            break;
                                        case "GBP":
                                            this._products.products[p].ccy_symbol = "£";
                                            this._products.products[p].rep_ccy = "GBP";
                                            break;
                                        case "EUR":
                                            this._products.products[p].ccy_symbol = "€";
                                            this._products.products[p].rep_ccy = "EUR";
                                            break;
                                        case "CNY":
                                            this._products.products[p].ccy_symbol = "¥";
                                            this._products.products[p].rep_ccy = "CNY";
                                            break;
                                        case "JPY":
                                            this._products.products[p].ccy_symbol = "¥";
                                            this._products.products[p].rep_ccy = "JPY";
                                            break;
                                    }
                                }
                            }
                        }
                        else if (input.fund_id) {
                            for (var f = 0; f < this._funds.funds.length; f++) {
                                if (input.name === this._funds.funds[f].name) {
                                    this._funds.funds[f].rep_value = numeral(this.rep_ccy.value.toFixed(2)).format('0,0.00');
                                    this._funds.funds[f].value = numeral(this._funds.funds[f].value).format('0,0.00');
                                    switch (ccy) {
                                        case "ZAR":
                                            this._funds.funds[f].ccy_symbol = "R";
                                            this._funds.funds[f].rep_ccy = "ZAR";
                                            break;
                                        case "USD":
                                            this._funds.funds[f].ccy_symbol = "$";
                                            this._funds.funds[f].rep_ccy = "USD";
                                            break;
                                        case "GBP":
                                            this._funds.funds[f].ccy_symbol = "£";
                                            this._funds.funds[f].rep_ccy = "GBP";
                                            break;
                                        case "EUR":
                                            this._funds.funds[f].ccy_symbol = "€";
                                            this._funds.funds[f].rep_ccy = "EUR";
                                            break;
                                        case "CNY":
                                            this._funds.funds[f].ccy_symbol = "¥";
                                            this._funds.funds[f].rep_ccy = "CNY";
                                            break;
                                        case "JPY":
                                            this._funds.funds[f].ccy_symbol = "¥";
                                            this._funds.funds[f].rep_ccy = "JPY";
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    if (input.client_id) {
                        if (ccy === input.base_ccy) {
                            this._userService.getClients()
                                .subscribe(function (data) {
                                for (var i = 0; i < data.clients.length; i++) {
                                    if (data.clients[i].client_id === input.client_id) {
                                        _this._clients.clients[i] = data.clients[i];
                                        _this._clients.clients[i].value = numeral(_this._clients.clients[i].value).format('0,0.00');
                                        _this._clients.clients[i].rep_ccy = data.clients[i].base_ccy;
                                        _this._clients.clients[i].rep_value = numeral(_this._clients.clients[i].value).format('0,0.00');
                                    }
                                }
                            }, function (error) { return alert(JSON.stringify(error)); });
                        }
                    }
                    else if (input.product_id) {
                        if (ccy === input.base_ccy) {
                            this._userService.getProducts()
                                .subscribe(function (data) {
                                for (var i = 0; i < data.products.length; i++) {
                                    if (data.products[i].product_id === input.product_id) {
                                        _this._products.products[i] = data.products[i];
                                        _this._products.products[i].value = numeral(_this._products.products[i].value).format('0,0.00');
                                        _this._products.products[i].rep_ccy = data.products[i].base_ccy;
                                        _this._products.products[i].rep_value = numeral(_this._products.products[i].value).format('0,0.00');
                                    }
                                }
                            }, function (error) { return alert(JSON.stringify(error)); });
                        }
                    }
                    else if (input.fund_id) {
                        if (ccy === input.base_ccy) {
                            this._userService.getFunds()
                                .subscribe(function (data) {
                                for (var i = 0; i < data.funds.length; i++) {
                                    if (data.funds[i].fund_id === input.fund_id) {
                                        _this._funds.funds[i] = data.funds[i];
                                        _this._funds.funds[i].value = numeral(_this._funds.funds[i].value).format('0,0.00');
                                        _this._funds.funds[i].rep_ccy = data.funds[i].base_ccy;
                                        _this._funds.funds[i].rep_value = numeral(_this._funds.funds[i].value).format('0,0.00');
                                    }
                                }
                            }, function (error) { return alert(JSON.stringify(error)); });
                        }
                    }
                };
                UserProfile = __decorate([
                    core_1.Component({
                        templateUrl: 'app/user/userprofile.html',
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [http_1.HTTP_PROVIDERS]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], UserProfile);
                return UserProfile;
            }());
            exports_1("UserProfile", UserProfile);
        }
    }
});
//# sourceMappingURL=userprofile.js.map