System.register(['angular2/core', 'angular2/router', '../services/user.service'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, user_service_1;
    var productDetailsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            productDetailsComponent = (function () {
                function productDetailsComponent(_userService, _router) {
                    this._userService = _userService;
                    this._router = _router;
                    this.product_clients = { clients: [] };
                    this._products = { products: [] };
                    this._clients = { client_id: [] };
                }
                productDetailsComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._product = this._userService.getProductInfo();
                    this._userService.getClientProducts()
                        .subscribe(function (data) {
                        for (var c = 0; c < data.products.length; c++) {
                            if (_this._product.product_id === data.products[c].product_id) {
                                _this._clients.client_id.push(data.products[c]);
                            }
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                    this._userService.getClients()
                        .subscribe(function (data) {
                        for (var c = 0; c < data.clients.length; c++) {
                            for (var i = 0; i < _this._clients.client_id.length; i++) {
                                if (_this._clients.client_id[i].client_id === data.clients[c].client_id) {
                                    _this.product_clients.clients.push(data.clients[c]);
                                    _this.product_clients.clients[i].value = numeral(_this.product_clients.clients[i].value).format('0,0.00');
                                }
                            }
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                };
                productDetailsComponent.prototype.onClient = function (client) {
                    this._userService.storeClient(client);
                    this._router.navigate(['Client Details']);
                };
                productDetailsComponent = __decorate([
                    core_1.Component({
                        selector: 'product-details',
                        templateUrl: 'app/user/product-details/product-details.html',
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], productDetailsComponent);
                return productDetailsComponent;
            }());
            exports_1("productDetailsComponent", productDetailsComponent);
        }
    }
});
//# sourceMappingURL=product-details.component.js.map