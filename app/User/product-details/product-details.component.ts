import {Component, OnInit} from 'angular2/core';
import {RouterLink, ROUTER_DIRECTIVES, Router} from 'angular2/router';
import {UserService} from '../services/user.service';

@Component({
	selector: 'product-details',
	templateUrl: 'app/user/product-details/product-details.html',
	directives: [ROUTER_DIRECTIVES]
})
 
 export class productDetailsComponent implements OnInit{
 	public routes;
	private product_clients = { clients: [] } ;
	private _products = { products: [] };
	private _clients = { client_id: [] };
	private _product;

 	constructor(private _userService: UserService, private _router: Router){}
	
	 ngOnInit():any {
		this._product = this._userService.getProductInfo();
		
		this._userService.getClientProducts()
			.subscribe(
				data => {
				 	for(var c = 0; c < data.products.length; c++){
						if( this._product.product_id === data.products[c].product_id ){
							this._clients.client_id.push(data.products[c]);
						}
					}
				},
				error => alert(JSON.stringify(error))
			);
		this._userService.getClients()
			.subscribe(
				data => {
					for (var c = 0; c < data.clients.length; c++) {
						for (var i = 0; i < this._clients.client_id.length; i++) {
							if( this._clients.client_id[i].client_id === data.clients[c].client_id ){
								this.product_clients.clients.push(data.clients[c]);
								this.product_clients.clients[i].value = numeral(this.product_clients.clients[i].value).format('0,0.00');
							}
						}
					}
				},
				error => alert(JSON.stringify(error))
			);
	}
	onClient(client){
        this._userService.storeClient(client);
        this._router.navigate(['Client Details']);
    }
 	
 }