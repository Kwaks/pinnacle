import {Component, OnInit} from 'angular2/core';
import {RouterLink, ROUTER_DIRECTIVES, Router} from 'angular2/router';
import {UserService} from '../services/user.service';

@Component({
	selector: 'myproducts',
	templateUrl: 'app/user/products/products.html',
	directives: [ROUTER_DIRECTIVES]
})

export class ProductsComponent implements OnInit{
	private _products = { products: [] };
	
	constructor(private _userService: UserService, private _router: Router){}
	
	ngOnInit():any{
		this._userService.getProducts()
			.subscribe(
				data => this._products.products = data.products,
				error => alert(JSON.stringify(error)),
				() => {
					for (var i = 0; i < this._products.products.length; i++) {
						this._products.products[i].value = numeral(this._products.products[i].value).format('0,0.00');
					}
				}
			);
	}
	
    onProduct(product:any){
        this._userService.storeProduct(product);
        this._router.navigate(['Product Details']);
    }
}