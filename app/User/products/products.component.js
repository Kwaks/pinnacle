System.register(['angular2/core', 'angular2/router', '../services/user.service'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, user_service_1;
    var ProductsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            ProductsComponent = (function () {
                function ProductsComponent(_userService, _router) {
                    this._userService = _userService;
                    this._router = _router;
                    this._products = { products: [] };
                }
                ProductsComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._userService.getProducts()
                        .subscribe(function (data) { return _this._products.products = data.products; }, function (error) { return alert(JSON.stringify(error)); }, function () {
                        for (var i = 0; i < _this._products.products.length; i++) {
                            _this._products.products[i].value = numeral(_this._products.products[i].value).format('0,0.00');
                        }
                    });
                };
                ProductsComponent.prototype.onProduct = function (product) {
                    this._userService.storeProduct(product);
                    this._router.navigate(['Product Details']);
                };
                ProductsComponent = __decorate([
                    core_1.Component({
                        selector: 'myproducts',
                        templateUrl: 'app/user/products/products.html',
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], ProductsComponent);
                return ProductsComponent;
            }());
            exports_1("ProductsComponent", ProductsComponent);
        }
    }
});
//# sourceMappingURL=products.component.js.map