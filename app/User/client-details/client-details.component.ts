import {Component, OnInit} from 'angular2/core';
import {UserService} from '../services/user.service';
import {RouterLink, ROUTER_DIRECTIVES, Router} from 'angular2/router';

@Component({
	selector: 'client-details',
	templateUrl: 'app/user/client-details/client-details.html',
	directives: [ROUTER_DIRECTIVES]
})

export class clientDetailsComponent implements OnInit {
	public routes;
	private _client = {
		id: '',
		name: '',
		ccy: '',
		portfolio_value: 0,
		products : []
	};

	constructor(private _userService: UserService, private _router: Router) {}

	ngOnInit(): any {
		let client = this._userService.getClientInfo();
		this._userService.getClients()
			.subscribe(
				data => {
					for (var i = 0; i < data.clients.length; i++) {
						if (data.clients[i].client_id == client.client_id){
							this._client.id = data.clients[i].client_id;
							this._client.name = data.clients[i].name;
							this._client.ccy = data.clients[i].base_ccy;
							this._client.portfolio_value = numeral(data.clients[i].value).format('0,0.00');
						}
					}
				},
				error => alert(JSON.stringify(error))
			);

		this._userService.getClientProducts()
			.subscribe(
				data => {
					for(var c = 0; c < data.products.length; c++) {
						if (data.products[c].client_id == this._client.id){
							this._client.products.push(data.products[c]);
						}
					}
				},
				error => alert("We have a problem here: "+ JSON.stringify(error)),
				() => {
						if(this._client.products.length > 0){
							for(var i = 0; i < this._client.products.length; i++){
								this._client.products[i].value = numeral(this._client.products[i].value).format('0,0.00');	
							}
						}
					}
			);
	}

	onProduct(product){
		this._userService.storeProduct(product);
		this._router.navigate(['Fund Details']);
	}
}