System.register(['angular2/core', '../services/user.service', 'angular2/router'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_service_1, router_1;
    var clientDetailsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            clientDetailsComponent = (function () {
                function clientDetailsComponent(_userService, _router) {
                    this._userService = _userService;
                    this._router = _router;
                    this._client = {
                        id: '',
                        name: '',
                        ccy: '',
                        portfolio_value: 0,
                        products: []
                    };
                }
                clientDetailsComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    var client = this._userService.getClientInfo();
                    this._userService.getClients()
                        .subscribe(function (data) {
                        for (var i = 0; i < data.clients.length; i++) {
                            if (data.clients[i].client_id == client.client_id) {
                                _this._client.id = data.clients[i].client_id;
                                _this._client.name = data.clients[i].name;
                                _this._client.ccy = data.clients[i].base_ccy;
                                _this._client.portfolio_value = numeral(data.clients[i].value).format('0,0.00');
                            }
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                    this._userService.getClientProducts()
                        .subscribe(function (data) {
                        for (var c = 0; c < data.products.length; c++) {
                            if (data.products[c].client_id == _this._client.id) {
                                _this._client.products.push(data.products[c]);
                            }
                        }
                    }, function (error) { return alert("We have a problem here: " + JSON.stringify(error)); }, function () {
                        if (_this._client.products.length > 0) {
                            for (var i = 0; i < _this._client.products.length; i++) {
                                _this._client.products[i].value = numeral(_this._client.products[i].value).format('0,0.00');
                            }
                        }
                    });
                };
                clientDetailsComponent.prototype.onProduct = function (product) {
                    this._userService.storeProduct(product);
                    this._router.navigate(['Fund Details']);
                };
                clientDetailsComponent = __decorate([
                    core_1.Component({
                        selector: 'client-details',
                        templateUrl: 'app/user/client-details/client-details.html',
                        directives: [router_1.ROUTER_DIRECTIVES]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], clientDetailsComponent);
                return clientDetailsComponent;
            }());
            exports_1("clientDetailsComponent", clientDetailsComponent);
        }
    }
});
//# sourceMappingURL=client-details.component.js.map