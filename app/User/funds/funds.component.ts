import {Component, OnInit} from 'angular2/core';
import {RouterLink, ROUTER_DIRECTIVES, Router} from 'angular2/router';
import {UserService} from '../services/user.service';

@Component({
	selector: 'my-funds',
	templateUrl: 'app/user/funds/funds.html',
    directives: [ROUTER_DIRECTIVES]
})

export class FundsComponent implements OnInit{
	private _funds = { funds: [] };
	
	constructor(private _userService: UserService, private _router: Router){}
	
	ngOnInit():any{
		this._userService.getFunds()
			.subscribe(
				data => {
					this._funds.funds = data.funds;
				},
				error => alert(JSON.stringify(error)),
				() => {
					for(var i = 0; i < this._funds.funds.length; i++){
						this._funds.funds[i].value = numeral(this._funds.funds[i].value).format('0,0.00');
					}
				}
			);
	}
	
    onFund(fund:any){
        this._userService.storeFund(fund);
        this._router.navigate(["Fund Clients"]);
    }
}