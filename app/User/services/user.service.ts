import {Injectable} from 'angular2/core';
import {Http, Response, Headers} from 'angular2/http';
import {Observable}  from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class UserService {

	constructor(private _http: Http){
	}

	private _userUrl = 'mocks/user.json';
	private _usersUrl = 'mocks/users.json';

	private _clients = 'mocks/wm_clients.json';
	private _products = 'mocks/wm_products.json';
	private _funds = 'mocks/wm_funds.json';
	private _transactions = 'mocks/wm_transactions.json';
	
	private _client = "mocks/zuikertop_client.json";
	private _clientProducts = "mocks/client_products.json";
	private _productFunds = "mocks/client_product_funds.json";
	private _fundTransactions = "mocks/client_product_fund_transactions.json";

	private _ccyRates = "mocks/ccy_rates.json";
	private ccy_rates = null;
	private Rates = {
		 
	};

/*
	WM/TA get all their clients, Products, Funds and Transactions
*/
	getClients(){
		return this._http.get(this._clients)
			.map(res => res.json())
			.catch(error => this.handleError(error));
	}
	getProducts(){
		return this._http.get(this._products)
			.map(res => res.json())
			.catch(error => this.handleError(error));
	}
	getFunds(){
		return this._http.get(this._funds)
			.map(res => res.json())
			.catch(error => this.handleError(error));
	}
	getTransactions(){
		return this._http.get(this._transactions)
			.map(res => res.json())
			.catch(error => this.handleError(error));
	}

/*
	Convert between various currencies e.g USD => ZAR, ZAR => EUR
*/
	getCCYRates(){
		return this._http.get(this._ccyRates)
			.map(res => res.json())
			.catch(error => this.handleError(error));
	}

/*
	Store/Get the a Client info, Product Info and Fund info 
*/
	storeClient(client){
		window.sessionStorage.setItem("client", JSON.stringify(client));
	}
	getClientInfo(){
		let client = JSON.parse(window.sessionStorage.getItem("client"));
		return client;
	}
	removeClient(){
		window.sessionStorage.removeItem("client");
	}
	
	storeProduct(product){
		window.sessionStorage.setItem("product", JSON.stringify(product));
	}
	getProductInfo(){
		let product = JSON.parse(window.sessionStorage.getItem("product"));
		return product;
	}
	removeProduct(){
		window.sessionStorage.removeItem("product");
	}
	
	storeFund(fund){
		window.sessionStorage.setItem("fund", JSON.stringify(fund));
	}
	getFundInfo(){
		let fund = JSON.parse(window.sessionStorage.getItem("fund"));
		return fund;
	}
	removeFund(){
		window.sessionStorage.removeItem("fund");
	}

/*
	Get Client Info, Client's Produts, Product's Funds and Fund's Transactions
*/
	getClient(){
		return this._http.get(this._clients)
			.map(res => res.json())
			.catch(error => this.handleError(error));
	}
	getClientProducts(){
		return this._http.get(this._clientProducts)
			.map(res => res.json())
			.catch(error => this.handleError(error));
	}
	getProductFunds(){
		return this._http.get(this._productFunds)
			.map(res => res.json())
			.catch(error => this.handleError(error));
	}
	getFundTransactions(){
		return this._http.get(this._fundTransactions)
			.map(res => res.json())
			.catch(error => this.handleError(error));
	}

/*
	Get all Users, User Info, Cookie and Error Handling
*/
	getUsers() {
		return this._http.get(this._usersUrl)
			.map(res => <User[]> res.json())
			//.do(data => console.log(data))
			.catch(this.handleError)
	}

	getUser(domain:String, adusername:String){
		return this._http.get(this._userUrl)
			.map(res => <User> res.json())
			.catch(error => this.handleError(error));
	}

	getCookie(name){
		let value = "; " + document.cookie;
		let parts = value.split("; " + name + "=");
		if(parts.length == 2){
			return parts.pop().split(";").shift();
		}
	}

	private handleError(error:Response){
		// in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.log(JSON.stringify(error));
        if(error.status == 400){
        	console.log("Handle 403");
        	window.location.href = "/pinnacle/";
        	return Observable.throw ("403 Forbidden");
        }
        return Observable.throw(error.json().error || 'Server error');
	}
}