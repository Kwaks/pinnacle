System.register(['angular2/core', 'angular2/http', 'rxjs/Observable', 'rxjs/Rx'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, Observable_1;
    var UserService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (Observable_1_1) {
                Observable_1 = Observable_1_1;
            },
            function (_1) {}],
        execute: function() {
            UserService = (function () {
                function UserService(_http) {
                    this._http = _http;
                    this._userUrl = 'mocks/user.json';
                    this._usersUrl = 'mocks/users.json';
                    this._clients = 'mocks/wm_clients.json';
                    this._products = 'mocks/wm_products.json';
                    this._funds = 'mocks/wm_funds.json';
                    this._transactions = 'mocks/wm_transactions.json';
                    this._client = "mocks/zuikertop_client.json";
                    this._clientProducts = "mocks/client_products.json";
                    this._productFunds = "mocks/client_product_funds.json";
                    this._fundTransactions = "mocks/client_product_fund_transactions.json";
                    this._ccyRates = "mocks/ccy_rates.json";
                    this.ccy_rates = null;
                    this.Rates = {};
                }
                /*
                    WM/TA get all their clients, Products, Funds and Transactions
                */
                UserService.prototype.getClients = function () {
                    var _this = this;
                    return this._http.get(this._clients)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                UserService.prototype.getProducts = function () {
                    var _this = this;
                    return this._http.get(this._products)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                UserService.prototype.getFunds = function () {
                    var _this = this;
                    return this._http.get(this._funds)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                UserService.prototype.getTransactions = function () {
                    var _this = this;
                    return this._http.get(this._transactions)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                /*
                    Convert between various currencies e.g USD => ZAR, ZAR => EUR
                */
                UserService.prototype.getCCYRates = function () {
                    var _this = this;
                    return this._http.get(this._ccyRates)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                /*
                    Store/Get the a Client info, Product Info and Fund info
                */
                UserService.prototype.storeClient = function (client) {
                    window.sessionStorage.setItem("client", JSON.stringify(client));
                };
                UserService.prototype.getClientInfo = function () {
                    var client = JSON.parse(window.sessionStorage.getItem("client"));
                    return client;
                };
                UserService.prototype.removeClient = function () {
                    window.sessionStorage.removeItem("client");
                };
                UserService.prototype.storeProduct = function (product) {
                    window.sessionStorage.setItem("product", JSON.stringify(product));
                };
                UserService.prototype.getProductInfo = function () {
                    var product = JSON.parse(window.sessionStorage.getItem("product"));
                    return product;
                };
                UserService.prototype.removeProduct = function () {
                    window.sessionStorage.removeItem("product");
                };
                UserService.prototype.storeFund = function (fund) {
                    window.sessionStorage.setItem("fund", JSON.stringify(fund));
                };
                UserService.prototype.getFundInfo = function () {
                    var fund = JSON.parse(window.sessionStorage.getItem("fund"));
                    return fund;
                };
                UserService.prototype.removeFund = function () {
                    window.sessionStorage.removeItem("fund");
                };
                /*
                    Get Client Info, Client's Produts, Product's Funds and Fund's Transactions
                */
                UserService.prototype.getClient = function () {
                    var _this = this;
                    return this._http.get(this._clients)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                UserService.prototype.getClientProducts = function () {
                    var _this = this;
                    return this._http.get(this._clientProducts)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                UserService.prototype.getProductFunds = function () {
                    var _this = this;
                    return this._http.get(this._productFunds)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                UserService.prototype.getFundTransactions = function () {
                    var _this = this;
                    return this._http.get(this._fundTransactions)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                /*
                    Get all Users, User Info, Cookie and Error Handling
                */
                UserService.prototype.getUsers = function () {
                    return this._http.get(this._usersUrl)
                        .map(function (res) { return res.json(); })
                        .catch(this.handleError);
                };
                UserService.prototype.getUser = function (domain, adusername) {
                    var _this = this;
                    return this._http.get(this._userUrl)
                        .map(function (res) { return res.json(); })
                        .catch(function (error) { return _this.handleError(error); });
                };
                UserService.prototype.getCookie = function (name) {
                    var value = "; " + document.cookie;
                    var parts = value.split("; " + name + "=");
                    if (parts.length == 2) {
                        return parts.pop().split(";").shift();
                    }
                };
                UserService.prototype.handleError = function (error) {
                    // in a real world app, we may send the server to some remote logging infrastructure
                    // instead of just logging it to the console
                    console.log(JSON.stringify(error));
                    if (error.status == 400) {
                        console.log("Handle 403");
                        window.location.href = "/pinnacle/";
                        return Observable_1.Observable.throw("403 Forbidden");
                    }
                    return Observable_1.Observable.throw(error.json().error || 'Server error');
                };
                UserService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], UserService);
                return UserService;
            }());
            exports_1("UserService", UserService);
        }
    }
});
//# sourceMappingURL=user.service.js.map