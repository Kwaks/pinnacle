System.register(["angular2/core", "angular2/router", '../services/user.service'], function(exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, user_service_1;
    var FundDetailsComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (user_service_1_1) {
                user_service_1 = user_service_1_1;
            }],
        execute: function() {
            FundDetailsComponent = (function () {
                function FundDetailsComponent(_userService, _router) {
                    this._userService = _userService;
                    this._router = _router;
                    this._funds = {
                        client_id: '',
                        client_name: '',
                        product: '',
                        product_id: '',
                        funds: []
                    };
                }
                FundDetailsComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    var client = this._userService.getClientInfo();
                    this._userService.getClients()
                        .subscribe(function (data) {
                        for (var i = 0; i < data.clients.length; i++) {
                            if (data.clients[i].client_id == client.client_id) {
                                _this._funds.client_id = data.clients[i].client_id;
                                _this._funds.client_name = data.clients[i].name;
                            }
                        }
                    }, function (error) { return alert(JSON.stringify(error)); });
                    var product = this._userService.getProductInfo();
                    this._funds.product = product.name;
                    this._userService.getProductFunds()
                        .subscribe(function (data) {
                        for (var i = 0; i < data.funds.length; i++) {
                            if (data.funds[i].product_id === product.product_id && data.funds[i].client_id === _this._funds.client_id) {
                                _this._funds.funds.push(data.funds[i]);
                            }
                        }
                    }, function (error) { return console.log(JSON.stringify(error)); }, function () {
                        for (var i = 0; i < _this._funds.funds.length; i++) {
                            if (_this._funds.funds.length > 0) {
                                _this._funds.funds[i].value = numeral(_this._funds.funds[i].value).format('0,0.00');
                            }
                        }
                    });
                    var fund = this._userService.getFundInfo();
                };
                FundDetailsComponent.prototype.onFund = function (fund) {
                    this._userService.storeFund(fund);
                    this._router.navigate(['Transaction Details']);
                };
                FundDetailsComponent = __decorate([
                    core_1.Component({
                        selector: "fund-details",
                        templateUrl: "app/user/fund-details/fund-details.html",
                        directives: [router_1.ROUTER_DIRECTIVES],
                        providers: [user_service_1.UserService]
                    }), 
                    __metadata('design:paramtypes', [user_service_1.UserService, router_1.Router])
                ], FundDetailsComponent);
                return FundDetailsComponent;
            }());
            exports_1("FundDetailsComponent", FundDetailsComponent);
        }
    }
});
//# sourceMappingURL=fund-details.component.js.map