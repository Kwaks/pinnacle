import {Component, OnInit} from "angular2/core";
import {RouterLink, ROUTER_DIRECTIVES, Router} from "angular2/router";
import {UserService} from '../services/user.service';

@Component({
	selector: "fund-details",
	templateUrl: "app/user/fund-details/fund-details.html",
    directives: [ROUTER_DIRECTIVES],
    providers: [UserService]
})

export class FundDetailsComponent implements OnInit{
	
	private _funds = {
		client_id: '',
		client_name: '',
		product: '',
		product_id: '',
		funds: []
	}

	constructor(private _userService: UserService, private _router: Router){}

	ngOnInit():any{
		let client = this._userService.getClientInfo();
		this._userService.getClients()
			.subscribe(
				data => {
					for (var i = 0; i < data.clients.length; i++) {
						if (data.clients[i].client_id == client.client_id){
							this._funds.client_id = data.clients[i].client_id;
							this._funds.client_name = data.clients[i].name;
						}
					}
				},
				error => alert(JSON.stringify(error))
			);
		let product = this._userService.getProductInfo();
		this._funds.product = product.name;
		this._userService.getProductFunds()
			.subscribe(
				data => {
					for(var i = 0; i < data.funds.length; i++){
						if (data.funds[i].product_id === product.product_id && data.funds[i].client_id === this._funds.client_id){
							this._funds.funds.push(data.funds[i]);
						}
					}
				},
				error => console.log(JSON.stringify(error)),
				() => {
						for (var i = 0; i < this._funds.funds.length; i++) {
							if(this._funds.funds.length > 0){
								this._funds.funds[i].value = numeral(this._funds.funds[i].value).format('0,0.00');
							}
						}
						
				}
			);
		let fund = this._userService.getFundInfo();
	}

	onFund(fund){
		this._userService.storeFund(fund);
		this._router.navigate(['Transaction Details']);
	}
}