import {Component} from 'angular2/core';
import {RouterLink, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import {Routes, APP_ROUTES} from './routes/route.config';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    directives: [ROUTER_DIRECTIVES]
})
@RouteConfig(APP_ROUTES)
export class AppComponent { 
	public people = { name:''};

	search() {
		this.people.name = "Kwakhona Mahamba";
		alert(this.people.name);
	}
}